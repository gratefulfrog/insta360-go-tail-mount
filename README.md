# Insta360 GO : Tail, Fuselage, and Pod Mounts

Some 3D designs to mount the Insta30 Go camera on the tail of an RC plane.

The mount sits under the [frame](https://www.thingiverse.com/thing:3862949) designed 
by [crosoft](https://www.thingiverse.com/crosoft/about) and published on [thingiverse](https://www.thingiverse.com/).

This readme provides attribution as per the CC license.

Below, links to images:

* [Fuselage Mount](https://photos.app.goo.gl/qPyzsTFy3E8iUCWj8)
* [Tail Fin Mount](https://photos.app.goo.gl/fA8yoxKwwd4sLhgeA)
* [Sleek Pod Mount](https://youtu.be/qMcUxLiLDJk)


