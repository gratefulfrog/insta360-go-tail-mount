$fn=100;

cameraModelFileName = "../STL/insta360_go_0415.stl";
wrongMountFileName  = "../STL/Insta360_Go_Gopro_Mount_frame.stl";
dxfFileName         = "../DXF/wrongMount.dxf";
airfoilFIneName     = "../DXF/airfoil.dxf";
fuselageFileName    = "../DXF/fuselageMount.dxf";

//import(cameraModelFileName);
/*
projection()
rotate([90,0,0])
import(wrongMountFileName);
*/
//linear_extrude(20)
//import(dxfFileName,layer= "cut");

module rawMount() {
  difference(){
    translate([0,0,-6])
      import(wrongMountFileName);
    linear_extrude(20,center=true)
      import(dxfFileName,layer= "cut");
  }
}

module test0(){
  rawMount();
  linear_extrude(20,center=true)
      import(dxfFileName,layer= "new");
}

/*
include <chamfer_extrude.scad>; 

//chamfer_extrude(height=4, angle=15, $fn=16)text("S", size=24, font="EB Garamond:style=12 Italic", $fn=64);

chamfer_extrude(height=5, angle=25, $fn=16)
  translate([5,0,0,])scale([2,1,1])circle(20);
*/

cylRadius      = 1;
plateLength    = 30;
plateHeigt     =  20;
mountThickness = 8.2;
finWidth       = 10;
plateThickness = 3;
tongueThickness = 3;

module top(){
  difference(){
    hull(){
      linear_extrude(mountThickness,center=true)
        import (airfoilFIneName,layer="0");
      translate([0,-cylRadius,finWidth/2.])
        rotate([0,90,0])
          cylinder(plateLength,cylRadius,cylRadius);
      translate([0,-cylRadius,-finWidth/2.])
        rotate([0,90,0])
          cylinder(plateLength,cylRadius,cylRadius);
    }
    translate([0,-5,0])
      cube([100,10,100],center=true);
  }
}
  
//top();

module sideL(){
  translate([0,0,plateThickness/2.-finWidth/2.])
    rotate([-90,0,0])
      rotate([0,90,0])
        difference(){
          hull(){  
            cylinder(plateLength,plateThickness,plateThickness);
            translate([plateHeigt,0,0])
              cylinder(plateLength,plateThickness,plateThickness);
            rotate([0,90,0])  
              cylinder(plateHeigt,plateThickness,plateThickness);
            translate([0,0,plateLength])
              rotate([0,90,0])  
                cylinder(plateHeigt,plateThickness,plateThickness);
          }
          translate([-10,0,-10])
            rotate([0,0,-90])
              cube([5,100,100]);
        }
      }
      
module sideR(){
  mirror([0,0,1])
    sideL();
 }
 module side(){
  sideL();
  sideR();
 }
 
 module panel(){
  difference(){
   sideR();
   translate([-5,0,0])
    cube([40,10,10]);
  }
 }
//panel();
 
 module topWSideAndTongue(){  
  linear_extrude(tongueThickness,center=true)
      import (airfoilFIneName,layer="tongue");   
  difference(){
    hull(){
      top();
      side();
    }
    {
    translate([-5,-30,-3.5])
      cube([40,30,20]);
    linear_extrude(tongueThickness*10,center=true)
      import (airfoilFIneName,layer="cutter");    
    }
  }
}
mountXOffset = 20.6;
mountZOffset = 11.7;
mountZOffsetDiv2 = mountZOffset/2.;
mountYOffset = 3.0;

module positionMount(){
  translate([mountZOffsetDiv2,mountYOffset,0])
    rotate([0,0,90])
      rotate([-90,0,0])
        translate([mountXOffset+mountZOffsetDiv2,0,-mountZOffsetDiv2])
          import(wrongMountFileName);
}
module testAssembly(){
  rotate([90,0,0]){
    positionMount();
    topWSideAndTongue();
    //panel();
  }
}
//testAssembly();
//topWSideAndTongue();
//panel();

///////////////////////////////////////////////////////////
////////// Fuselage Mount /////////////////////////////////
///////////////////////////////////////////////////////////

defaultFuselageThickness = 6;
plateZ =  25;
tongueX = 11.7;
gap     = 10;

module getTongue(fuselageThickness, spacer, horizontal=true){  
  tranX = horizontal ? 0 : tongueX/2.;
  rotY   = horizontal ? 0 : 90;
  translate([tranX,0,0])
    rotate([0,rotY,0])
      translate([-tranX,0,0]){
        linear_extrude(tongueThickness,center=true)
          import(fuselageFileName,layer="tongue");
        translate([0,-fuselageThickness,-tongueThickness/2.])
          cube([tongueX,fuselageThickness,tongueThickness]);
        }
      }  
      
module fuselageMountTongueAndTip(fuselageThickness=6,horizontal=true){
  space = fuselageThickness;
  translate([0,0,space])
    rotate([90,0,0]){
      getTongue(fuselageThickness,space,horizontal);
      translate([0,-space,0])
        getPlate(space);
    }
}
module fmX(start=5,end=10){
  for(d=[start:1:end])
    translate([(plateZ+gap)*(d-start),0,0])
      fuselageMountTongueAndTip(d,true);
  }
  
module getPlate(spc){
  difference(){
    linear_extrude(plateZ,center=true)
      import(fuselageFileName,layer="plate");  
    translate([6,0,-plateZ/4.])
      rotate([90,0,0])
        linear_extrude(1,center=true)
          text(str(spc,"mm"),size = 6,halign="center",valign="center");
  }
}
//fuselageMountTongueAndTip();
fmX();