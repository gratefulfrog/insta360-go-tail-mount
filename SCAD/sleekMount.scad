//scale([1,1,5])
//resize([0,0,50],auto=[false,false,false])
//projection()
//rotate([0,0,0])

include <../../CameraBox/HooksAndPylon00.scad>

module shape0(){
  rotate([0,90,90]){
    linear_extrude(60,scale=0.2)
      import("../DXF/CameraProfile.dxf",layer = "0");
    translate([0,0,-15])
      linear_extrude(15)
        import("../DXF/CameraProfile.dxf",layer = "0");
  }
}
module shape1(){
  hull(){
    shape0();
    translate([0,20,-19.3])
      pylonTop0(pylonWidth,
                pylonTopHeight,
                0.7*pylonLenghtCenter2Center,
                pylonBaseHeight + pylonMiddleHeight,
                false);
  }
}

module hooks(){
  translate([0,20,-18.24])
    pylonTop0(pylonWidth,
              pylonTopHeight,
              0.7*pylonLenghtCenter2Center,
              pylonBaseHeight + pylonMiddleHeight,
              true);
}

module concept(){
  shape1();
  hooks();
}

module camera(hll=true){
  dy=-8;
  color("green")
    translate([-15.2,dy,0])
      rotate([90,90,0])
        if(hll){
          hull()
            import("../STL/insta360_go_0415.stl");
        }
        else{
          import("../STL/insta360_go_0415.stl");
        }
} 
// version 02 scaling
camXScale = 1.009;
camYScale = 1.0;
camZScale = 1.040;

module fullPod(){
  difference(){
    union(){
      hooks();
      scale([1.1,1.1,1.1])
        shape1();
    }
    scale([.9,0.9,0.9])
      shape1();
    scale([camXScale,camYScale,camZScale])
      camera();
    cutter();
  }
}

module cameraOutliner(xScale,yScale,zScale){
  projection()
    rotate([-90,0,0])
      scale([xScale,yScale,zScale])
        camera();
}

module cutter(){
  rotate([-90,0,0])
    scale([0.18,0.13,1])
    linear_extrude(70)
      cameraOutliner(camXScale,camYScale,camZScale);
      //cameraOutliner(camXScale);
}
fullPod();
//concept();
//camera(false);

//cameraOutliner(camXScale,camYScale,camZScale);